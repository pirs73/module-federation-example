require("dotenv").config()
const HtmlWebpackPlugin = require("html-webpack-plugin")
const {ModuleFederationPlugin} = require("webpack").container
const path = require("path")

const PUBLIC_PATH =
	process.env.NODE_ENV === "production"
		? process.env.REACT_APP_PUBLIC_PATH
		: "http://localhost:3010/"

console.log(PUBLIC_PATH)

module.exports = {
	entry: "./src/index",
	mode: "development",
	resolve: {
		extensions: [".js", ".jsx"]
	},
	devServer: {
		static: {
			directory: path.join(__dirname, "dist")
		},
		port: 3010,
		historyApiFallback: true // Add this line to enable client-side routing
	},
	output: {
		publicPath: `${PUBLIC_PATH}`
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)?$/,
				loader: "babel-loader",
				exclude: /node_modules/,
				options: {
					presets: ["@babel/preset-react"]
				}
			}
		]
	},
	plugins: [
		new ModuleFederationPlugin({
			name: "remote",
			library: {type: "var", name: "remote"},
			remotes: {app1: "app1"},
			filename: "remote.js",
			exposes: {
				"./SettingsProvider": "./src/SettingsProvider",
				"./Nav": "./src/components/Nav"
			},
			shared: {
				"@stitches/react": {
					singleton: true
				},
				react: {
					eager: true,
					singleton: true,
					version: "0",
					requiredVersion: false
				},
				"react-dom": {
					requiredVersion: false,
					singleton: true,
					version: "0"
				},
				swiper: {
					eager: true,
					singleton: true
				}
			}
		}),
		new HtmlWebpackPlugin({
			inject: false,
			template: "./public/index.html"
		})
	]
}
