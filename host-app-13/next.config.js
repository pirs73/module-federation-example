/** @type {import('next').NextConfig} */
const {NextFederationPlugin} = require("@module-federation/nextjs-mf")

const nextConfig = {
	webpack(config, options) {
		if (!options.isServer) {
			config.plugins.push(
				new NextFederationPlugin({
					name: "host",
					remotes: {
						app1: "app1@https://api.plan.quicks.ai/api/mf/remoteEntry.js?pathname=https%3A%2F%2Fplan.quicks.ai%2Fonboarding-delta%2F9ff0d52c-199f-4bd8-99f0-e01f472951eb"
					},
					filename: "static/chunks/remoteEntry.js"
				})
			)
		}

		config.experiments.buildHttp = {
			allowedUris: ["https://api.plan.quicks.ai/"]
		}

		return config
	}
}

module.exports = nextConfig
