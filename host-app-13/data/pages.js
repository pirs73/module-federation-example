export const examplePageData = {
  id: "9ff0d52c-199f-4bd8-99f0-e01f472951eb",
  name: "AgePicsPage",
  props: {
    logoUrl: "https://s3.eu-north-1.amazonaws.com/docs.sum/images/QuicksLogo.png",
    title: "Become the most interesting person in the room",
    text: "3 MINUTE QUIZ",
    buttons: [
      {
        title: "Age: 18-24 →",
        name: "age",
        value: "18-24",
        iconUrl: "https://storage.googleapis.com/onboarding-static/a0n1p91c14fhlcw7sje48elo5158?GoogleAccessId=onboarding-static-assets%40summarizer-375814.iam.gserviceaccount.com\u0026Expires=1731575152\u0026Signature=wi3RFA9Rn09lhXZsJQu0GDwoWoVYuhXxiUt81qrRhAugCTnIjmghOM3m%2FbWnceCuaHo6e%2BaW1dfbY2fa0yoISXJf5VnmT7S88VJVunyh94%2BZJ0jN73BXjw0h9AqprxmsbI7cP8Hp%2F9qj%2FT7WZujO54%2B6F%2B3QyNVR1uZYK7UUfTBd62JTNrB6jlXqKB%2F6E4CT1aMW5%2FKTDISwibj0dafYaPlDMH82Itx31QmATgm3baJMqvoL2WShwf79aiTubXPhmMiV6tpEt0M2%2FqhzkDIQ8fQY4BrNaG%2FxzCmjsaFqITC1eRfgZE6hUGQPIK7PZWtU6V5n6Sla9%2FlbahV2FXVpbA%3D%3D\u0026response-content-disposition=attachment%3B+filename%3D%22Image-3.png%22%3B+filename%2A%3DUTF-8%27%27Image-3.png\u0026response-content-type=image%2Fwebp"
      },
      {
        title: "Age: 25-34 →",
        name: "age",
        value: "25-34",
        iconUrl: "https://storage.googleapis.com/onboarding-static/6iofj9t8ttzp59xhw91uohhh471s?GoogleAccessId=onboarding-static-assets%40summarizer-375814.iam.gserviceaccount.com\u0026Expires=1731575152\u0026Signature=NW%2F0txNpY1sJuoDtK5t1bioaPDpJMBzhFpCGAi93ekwcvOHMdWkMXmcjQXlgQNxljVPobRLSJqunZY7weng5Xf8g4OMKAfaQxs4s86icz5Kq7LKdSCp1s%2BQR%2F%2Btem51BqwlICO5W6CMyK7Hzt8WM%2FbaJBlLejpuYV%2FoAWttCnXNvDS78fe3UV6gujabMdaVzoVAXiAcog3XBDy4OrvmpCaOJZSJ8cwMvpYiaMS4SlIILICvU%2FHgzDavLkGBkE5NPJLROeR9i7oeC8x9drBcjM6nqG9oekI0oogXSMaGKJpmZLFAdPAtKUWkuULkj%2F57g88rBu%2FqadZ7wwXLslipHkQ%3D%3D\u0026response-content-disposition=attachment%3B+filename%3D%22Image-2.png%22%3B+filename%2A%3DUTF-8%27%27Image-2.png\u0026response-content-type=image%2Fwebp"
      },
      {
        title: "Age: 35-44 →",
        name: "age",
        value: "35-44",
        iconUrl: "https://storage.googleapis.com/onboarding-static/j1432fy0k1xf309or7qey6wx7iq7?GoogleAccessId=onboarding-static-assets%40summarizer-375814.iam.gserviceaccount.com\u0026Expires=1731575152\u0026Signature=N4UcsHcdMoIaMGQAKMqNCrQYuaT2OXZSeg2khvTMxx%2F4lmZxYNOkaWW9U9%2BgQIWkjV8le2CJNEWRplL5iPE7TSo7OPcwqYoe2tNBPEXxnt8iKEsAJoHiP2IGXTXTpRhAsgZw742OtkURtaJ40Tx0d1X7djfPF%2BDgGvVow8uBWW2seUp2fryz418qHSEc0mC6cTYyxTlO6buCZQaN%2B%2F9hv3qinINrkKxw1gzsEJrFlfGz%2FJO8QjYtrd5b0SRmLJpJWg6VuowKh%2Ba0EXaqTYicliXegu25pnxYP%2B4EY6PAJlt%2B5zqTXWxY9jm6%2F1cIJlpTH61sAqoG9qyhaOM9g25%2FYA%3D%3D\u0026response-content-disposition=attachment%3B+filename%3D%22Image-1.png%22%3B+filename%2A%3DUTF-8%27%27Image-1.png\u0026response-content-type=image%2Fwebp"
      },
      {
        title: "Age: 45+ →",
        name: "age",
        value: "45+",
        iconUrl: "https://storage.googleapis.com/onboarding-static/7whxxadd49siqmezko2i6nzqwxgm?GoogleAccessId=onboarding-static-assets%40summarizer-375814.iam.gserviceaccount.com\u0026Expires=1731575152\u0026Signature=JBTaIZfsx24Cd1wjDB9iDNHYNtWu%2Fp9clk8FRa0xv88LiLC2LWH5guqx5v399DE9%2FFJo3hT6pzEx7l7KA4vYeO0rtq8KQONbRq0u0rk6nWv2Q7alkw%2BkQPNXwoD%2F%2B8wGWUYT6hTwqEb19460EosfALbfzt4CzLvkdfYQrEVhvKqxddyPPXniXNauwgTZLrS6xYfl4BAQCbDUowjk11ypdbkz9QtRSbtQs9n3lxqTo57ZF3Xg4Sxz8WNDP1PWbkDm3ELP%2B0rdCPXPvx7FU%2B6UDxIXw6Bi32nC5zERbnRN2HUcanaSvleCPR88%2FKI6epUHshkpZ530BkpSlWMWrUXAzA%3D%3D\u0026response-content-disposition=attachment%3B+filename%3D%22Image.png%22%3B+filename%2A%3DUTF-8%27%27Image.png\u0026response-content-type=image%2Fwebp"
      }
    ],
    layout_name: "QuicksLayout"
  },
  pageNum: 1,
  title: null,
  internalName: "Landing",
  settings: {},
  saveResultCode: null,
  nextPages: [
    {
      next_page_id: "a43ec024-782d-43a8-ac33-3a8528488af3",
      condition: "true"
    }
  ]
};