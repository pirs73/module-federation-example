import {Suspense, lazy, useEffect, useState} from "react"
import Link from "next/link"
import {examplePageData} from "../data/pages"

export async function getServerSideProps() {
	const res = await fetch(`https://jsonplaceholder.typicode.com/posts`)
	const projects = await res.json()

	return {props: {projects}}
}

export default function HomePage() {
	const [SettingsProvider, setSettingsProvider] = useState(null)
	const [CurrentComponent, setCurrentComponent] = useState(null)
	const [CurrentLayout, setCurrentLayout] = useState(null)

	useEffect(() => {
		if (typeof window !== "undefined") {
			const SettingsProvider = lazy(() =>
				import("app1/SettingsContext").then((module) => ({
					default: module.SettingsProvider
				}))
			)
			setSettingsProvider(SettingsProvider)
			setCurrentComponent(lazy(() => import("app1/AgePicsPage")))
			setCurrentLayout(lazy(() => import("app1/QuicksLayout")))
		}
	}, [])

	return (
		<div style={{padding: "2%"}}>
			<h1>Next JS and React SSR Page</h1>
			<Link
				href="/client"
				className="link"
			>
				Static page
			</Link>
			<br />

			<div
				className="app"
				style={{height: "100%"}}
			>
				<Suspense>
					{SettingsProvider && (
						<SettingsProvider settings={{}}>
							{CurrentComponent && CurrentLayout && (
								<CurrentLayout
									component={
										<CurrentComponent
											{...examplePageData.props}
										/>
									}
								/>
							)}
						</SettingsProvider>
					)}
				</Suspense>
			</div>
		</div>
	)
}
