import React, {Suspense, lazy} from "react"
import CurrentLayout from "./CurrentLayout"
import CurrentComponent from "./CurrentComponent"
import SettingsProvider from "./SettingsProvider"

import {examplePageData} from "./data/pages"

const App = () => {
	return (
		<div
			className="app"
			style={{height: "100%"}}
		>
			<Suspense>
				<SettingsProvider settings={{}}>
					{CurrentComponent && CurrentComponent && (
						<CurrentLayout
							component={
								<CurrentComponent {...examplePageData.props} />
							}
						/>
					)}
				</SettingsProvider>
			</Suspense>
		</div>
	)
}

export default App
