import {Suspense, lazy, useEffect, useState} from "react"
import Script from "next/script"
import Link from "next/link"
import {examplePageData} from "../data/pages"

const Client = () => {
	const [SettingsProvider, setSettingsProvider] = useState(null)
	const [CurrentComponent, setCurrentComponent] = useState(null)
	const [CurrentLayout, setCurrentLayout] = useState(null)

	useEffect(() => {
		if (typeof window !== "undefined") {
			const SettingsProvider = lazy(() =>
				import("app1/SettingsContext").then((module) => ({
					default: module.SettingsProvider
				}))
			)
			setSettingsProvider(SettingsProvider)
			setCurrentComponent(lazy(() => import("app1/AgePicsPage")))
			setCurrentLayout(lazy(() => import("app1/QuicksLayout")))
		}
	}, [])

	return (
		<>
			<Script
				src="https://api.plan.quicks.ai/api/mf/remoteEntry.js?pathname=https%3A%2F%2Fplan.quicks.ai%2Fonboarding-delta%2F9ff0d52c-199f-4bd8-99f0-e01f472951eb"
				strategy="beforeInteractive"
			/>
			<div style={{padding: "2%"}}>
				<h1>Next JS and React Static Page</h1>
				<Link
					href="/"
					className="link"
				>
					SSR page
				</Link>
				<br />
				<div
					className="app"
					style={{height: "100%"}}
				>
					<Suspense>
						{SettingsProvider && (
							<SettingsProvider settings={{}}>
								{CurrentComponent && CurrentLayout && (
									<CurrentLayout
										component={
											<CurrentComponent
												{...examplePageData.props}
											/>
										}
									/>
								)}
							</SettingsProvider>
						)}
					</Suspense>
				</div>
			</div>
		</>
	)
}

export default Client
